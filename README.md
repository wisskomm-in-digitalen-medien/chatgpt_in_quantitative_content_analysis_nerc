# ChatGPT’s Potential for Quantitative Content Analysis: Categorizing Actors in Public Debates

## Background

This repository provides data and scripts related to a study conducted by Hohenwalde, C., Leidecker-Sandmann, M., Promies, N., & Lehmkuhl, M. that was previously presented at the annual conference of the Science Communication Group of the DGPuK in 2024. The authors analyzed the extent to which ChatGPT can be used to identify and classify actor groups in newspaper articles. The study aims to evaluate ChatGPT's potential as a replacement for human coders in quantitative content analysis, focusing on actor categorization in public debates.

## Repository Structure

Data:
- [data/codierte_akteursnennungen_for_chatgpt_23_10_12.csv](data/codierte_akteursnennungen_for_chatgpt_23_10_12.csv): This CSV file contains actor coding performed by human coders.
- [data/artikel_all_issues_cleaned_23_07_16_sample.csv](data/artikel_all_issues_cleaned_23_07_16_sample.csv): This file contains the article sample for the comparison analysis between ChatGPT and the human coders. 
- [data/szenario_1_codebook.csv](data/szenario_1_codebook.csv), [szenario_2_optimized_prompt.csv](szenario_2_optimized_prompt.csv), [szenario_3_pipeline_chatgpt_3_5.csv](szenario_3_pipeline_chatgpt_3_5.csv) und [szenario_4_pipeline_chatgpt_4_turbo.csv](szenario_4_pipeline_chatgpt_4_turbo.csv): These files contain actor classifications output by ChatGPT across the four different scenarios presented at the conference "Science Communication in the Age of Artificial Intelligence".

Code:
- [Identifikation_Akteure.ipynb](Identifikation_Akteure.ipynb): This Jupyter Notebook contains the Python code for coding articles using ChatGPT. The prompt to be used, as well as the pipeline structure, can be selected. The script also includes code for evaluation, such as the calculation of confusion matrices and F1-scores.

## Further information
- Hohenwalde, C., Leidecker-Sandmann, M., Promies, N., & Lehmkuhl, M. (2024). ChatGPT’s Potential for Quantitative Content Analysis: Categorizing Actors in Public Debates. []()
- Hohenwalde, C., Leidecker-Sandmann, M., Promies, N., & Lehmkuhl, M. (Juni 2024). How well can ChatGPT replace human coders in quantitative content analysis? Presentation at the conference "Science Communication in the Age of Artificial Intelligence", organized by DGPuK-Fachgruppe Wissenschaftskommunikation, Zürich. [https://publikationen.bibliothek.kit.edu/1000171605](https://publikationen.bibliothek.kit.edu/1000171605)